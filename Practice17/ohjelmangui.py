import tkinter
import tkinter.messagebox


class mygui:
    def __init__(self):
        self.mainwindows = tkinter.Tk()

        #Kaksi kehystÃ¤ eri arvoille
        self.top_frame = tkinter.Frame(self.mainwindows)
        self.bottom_frame = tkinter.Frame(self.mainwindows)

        #Luodaan boxi joka kysyy tekstiÃ¤ ylÃ¤frameen
        self.teksti = tkinter.Label(self.top_frame, text = 'Anna vittu mitÃ¤ vaan')
        #Samaan freimiin mieluusti sitten se kilometriboxi
        self.kilometriboxi = tkinter.Entry(self.top_frame, width = 10)

        #YlÃ¤kehys kuntoon
        self.teksti.pack(side='top')
        self.kilometriboxi.pack(side='top')
        #AlakehyksessÃ¤ mailiboxi
        self.mailiboxi = tkinter.Label(self.bottom_frame,text='Vittu mitÃ¤ vaan')
        self.variaabeliteksti = tkinter.StringVar()
        #Sitten itse teksti jossa tulee lukemaan tuo stringvariable:
        self.mailienmaara = tkinter.Label(self.bottom_frame,textvariable=self.variaabeliteksti)
        #Samaan settiin nappi
        self.laheta = tkinter.Button(self.bottom_frame,text='LÃ¤hetÃ¤',command=self.muunna)

        #PÃ¤kÃ¤tÃ¤Ã¤n bottom frame yhteen
        self.mailiboxi.pack(side='top')
        self.mailienmaara.pack(side='top')
        self.laheta.pack(side='top')
        self.top_frame.pack()
        self.bottom_frame.pack()
        tkinter.mainloop()
    def muunna(self):
        kilometrit = float(self.kilometriboxi.get())
        mailit = kilometrit * 0.6117
        self.variaabeliteksti.set(mailit)
def main():
    msgui = mygui()
main()
