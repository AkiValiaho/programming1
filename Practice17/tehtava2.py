import tkinter


class mygui:
    def __init__(self):
        self.mainwindows = tkinter.Tk()

        self.top_Frame = tkinter.Frame(self.mainwindows)
        self.middle_Frame = tkinter.Frame(self.mainwindows)
        self.frameforanswer = tkinter.Frame(self.mainwindows)
        self.bottom_Frame = tkinter.Frame(self.mainwindows)


        self.km_teksti= tkinter.Label(self.top_Frame, text = 'Montako kilometriä ajettu?')
        self.kulutus = tkinter.StringVar()
        self.kulutusteksti = tkinter.Label(self.frameforanswer,text='Auto kuluttaa ')
        self.kulutuslabel = tkinter.Label(self.frameforanswer,textvariable=self.kulutus)



        self.km_syote = tkinter.Entry(self.top_Frame, width = 10)

        self.litra_teksti= tkinter.Label(self.top_Frame, text = 'Montako litraa kulunut?')
        self.litra_syote = tkinter.Entry(self.top_Frame, width = 10)
        self.laskebutton= tkinter.Button(self.bottom_Frame, text='Laske',command =self.muunna)
        self.lopetabutton= tkinter.Button(self.bottom_Frame, text='Lopeta',command = self.mainwindows.destroy)


        self.km_teksti.pack(side='left')
        self.km_syote.pack(side='left')
        self.litra_teksti.pack(side='left')


        self.litra_syote.pack(side='left')
        self.kulutusteksti.pack(side='left')
        self.kulutuslabel.pack(side='left')
        self.laskebutton.pack(side='left')
        self.lopetabutton.pack(side='left')


        self.top_Frame.pack()
        self.middle_Frame.pack()
        self.frameforanswer.pack()
        self.bottom_Frame.pack()

        tkinter.mainloop()
    def muunna(self):
        litrat = float(self.litra_syote.get())
        kilometrit = float(self.km_syote.get())
        persata = kilometrit/100
        litratpersata = round(litrat/persata,2)
        self.kulutus.set(litratpersata)



def main():
    msgui = mygui()
main()
