import tkinter
import tkinter.messagebox


class mygui:
    def __init__(self):
        self.mainwindows = tkinter.Tk()

        self.top_frame = tkinter.Frame(self.mainwindows)
        self.bottom_frame = tkinter.Frame(self.mainwindows)
        #Väännetään ensin bottom-frameen kaksi buttonia
        #Tehdään siis ensiksi muutama stringvarri osoittelle jne.
        self.nimi = tkinter.StringVar()
        self.osoite = tkinter.StringVar()
        self.postinumero = tkinter.StringVar()
        #Sitten erikseen labelit
        self.nimiteksti = tkinter.Label(self.top_frame, textvariable= self.nimi)
        self.osoiteteksti = tkinter.Label(self.top_frame, textvariable= self.osoite)
        self.postinumeroteksti = tkinter.Label(self.top_frame, textvariable= self.postinumero)
        self.naytabutton = tkinter.Button(self.bottom_frame, text='Näytä',command=self.settexts)
        self.lopetabutton = tkinter.Button(self.bottom_frame, text='Lopeta',command = self.mainwindows.destroy)
        #Pakataan lopuksi jutut toimimaan
        self.nimiteksti.pack(side='top')
        self.osoiteteksti.pack(side='top')
        self.postinumeroteksti.pack(side='top')
        self.naytabutton.pack(side='top')
        self.lopetabutton.pack(side='top')
        #Pakataan freimit
        self.top_frame.pack()
        self.bottom_frame.pack()
        #Käynnistetään pääluuppi
        tkinter.mainloop()

    def settexts(self):
        self.nimi.set('Marjo Roponen')
        self.osoite.set('Keksittykatu 25')
        self.postinumero.set('7000 Kuopio')

def main():
    msgui = mygui()
main()
