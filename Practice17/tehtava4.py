import tkinter
import tkinter.messagebox


class mygui:
    def __init__(self):
        self.paa_ikkuna = tkinter.Tk()
        self.tiedotus = tkinter.Label(self.paa_ikkuna, text='Anna puhelun pituus minuutteina')
        self.tiedotusentry = tkinter.Entry(self.paa_ikkuna, width = 3)
        self.middlelabel = tkinter.Label(self.paa_ikkuna, text='Valitse soittoaika')
        self.paddinglabel = tkinter.Label(self.paa_ikkuna,text='')
        self.radiovalintamuut = tkinter.IntVar()
        self.radiovalintamuut.set(1)
        self.checkbutton1 = tkinter.Radiobutton(self.paa_ikkuna, text='Päivisin klo 6.00-17.59',variable=self.radiovalintamuut,value=1)
        self.checkbutton2 = tkinter.Radiobutton(self.paa_ikkuna,text='Iltaisin klo 18.00-23.59',variable=self.radiovalintamuut,value=2)
        self.checkbutton3= tkinter.Radiobutton(self.paa_ikkuna,text='Öisin klo 0.00-5.59',variable=self.radiovalintamuut,value=3)

        self.laskebutton= tkinter.Button(self.paa_ikkuna, text='Laske hinta', command = self.nayta_valinta)
        self.lopetabutton= tkinter.Button(self.paa_ikkuna, text='Lopeta', command = self.paa_ikkuna.destroy)



        #Grid-operaatiot (ei tarvitse spämmätä frameja)
        self.tiedotus.grid(row=0, column=0)
        self.tiedotusentry.grid(row=0, column=1)
        self.paddinglabel.grid(row=1,column=0) #grid-moduuli ignoree tyhjät rowit joten täytyy käyttää käsinväkerrettyä paddinglabelia tyhjään tilaan
        self.middlelabel.grid(row=2,column=0)
        #Tähän alle siis eri roweille noita checkbuttoneita!
        self.checkbutton1.grid(row=3,column=0)
        self.checkbutton2.grid(row=4,column=0)
        self.checkbutton3.grid(row=5,column=0)
        self.laskebutton.grid(row=6, column=0)
        self.lopetabutton.grid(row=6, column=1)


        #pääluuppi
        tkinter.mainloop()


    def nayta_valinta(self):
        x=int(self.tiedotusentry.get())
        if self.radiovalintamuut.get()==1:
            hinta= 0.07*x
        elif self.radiovalintamuut.get()==2:
            hinta= 0.12*x
        elif self.radiovalintamuut.get()==3:
            hinta= 0.05*x

        tkinter.messagebox.showinfo('Puhelun hinta ', 'Puhelun hinta on '  + format(hinta, '.2f') +' euroa')
def main():
    msgui = mygui()

main()
