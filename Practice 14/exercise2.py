from classes import Auto

def main():
    mercedes = Auto(1988,'Mercedes')
    kiihdytys = 5
    jarrutus = 5
    i = 0
    while i < 5:
            mercedes.kiihdyta()
            print("Auton vauhti tällähetkellä: {0}".format(mercedes.get__nopeus()))
            i+=1
    i = 0
    while i < jarrutus:
        mercedes.jarruta()
        print("Auton vauhti tällähetkellä: {0}".format(mercedes.get__nopeus()))
        i+=1


main()
