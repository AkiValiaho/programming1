from classes import Tyontekija, loaddictionary, addemployee,changeemployee,dumpdictionary
import os


def main():
    sanakirja = {}
    sanakirja = loaddictionary()
    continueflag = True
    print("Kuinka tÃ¤tÃ¤ ohjelmaa kÃ¤ytetÃ¤Ã¤n")
    print("Paina 1 etsiÃ¤ksesi tyÃ¶ntekijÃ¤Ã¤")
    print("Paina 2 lisÃ¤tÃ¤ksesi tyÃ¶ntekijÃ¤")
    print("Paina 3 muuttaaksesi tyÃ¶ntekijÃ¤n tietoja")
    print("Paina 4 poistaaksesi tyÃ¶ntekijÃ¤")
    print("Paina 5 listataksesi kaikki tyÃ¶ntekijÃ¤t")
    print("Paina 6 lopettaaksesi ohjelma")
    while continueflag == True:
        choice = int(input("Anna valintasi"))
        if choice == 1:
            etsittava = int(input("Anna etsittavan henkilÃ¶numero"))
            if etsittava in sanakirja:
                print("Sanakirjasta lÃ¶ytyi henkilÃ¶: {0}".format(etsittava))
                print("Nimi     Osasto      TyÃ¶tehtÃ¤vÃ¤")
                for value in sanakirja[etsittava]:
                        print(value,end=' ')
            else:
                print("Etsittävää henkilöä ei löytynyt sanakirjasta")
        elif choice == 2:
            sanakirja = addemployee(sanakirja)
        elif choice == 3:
            etsittava = int(input("Anna muutettavan henkilÃ¶numero:"))
            if etsittava in sanakirja:
                sanakirja = changeemployee(sanakirja,etsittava)
            else:
                print("Antamaasi henkilÃ¶numeroa ei lÃ¶ytynyt")
        elif choice == 4:
            etsittava = int(input("Anna poistettavan tyÃ¶ntekijÃ¤n henkilÃ¶numero:"))
            if etsittava in sanakirja:
                print("Sanakirjasta poistettu henkilÃ¶: {0}".format(etsittava))
                del sanakirja[etsittava]
            else:
                print("EtsittÃ¤vÃ¤ henkilÃ¶ ei lÃ¶ytynyt sanakirjasta")
        elif choice == 5:
            os.system('cls')
            for i in sanakirja.keys():
                print(sanakirja[i])
                print("\n-----------------------------------------------------------")

        elif choice ==  6:
            dumpdictionary(sanakirja)
            continueflag = False



main()
