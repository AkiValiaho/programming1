##############Tehty 4/5 tehtävää##############



#######classes.py##########
import pickle

class Lemmikki:
    def __init__(self,nimi,elainlaji,ika):
        self.__nimi = nimi
        self.__elainlaji = elainlaji
        self.__ika = ika
    def set__nimi(self,nimi):
        self.__nimi = nimi
    def set__elainlaji(self,elainlaji):
        self.__elainlaji = elainlaji
    def set_ika(self,ika):
        self.__ika = ika
    def get__nimi(self):
        return self.__nimi
    def get__elainlaji(self):
        return self.__elainlaji
    def get__ika(self):
        return self.__ika


class Auto:
    def __init__(self,vuosimalli,malli):
        self.__vuosimalli = vuosimalli
        self.__malli = malli
        self.__nopeus = 0
    def kiihdyta(self):
        self.__nopeus += 5
    def jarruta(self):
        self.__nopeus -= 5
    def get__nopeus(self):
        return self.__nopeus

class Tyontekija:
    def __init__(self,nimi,henkilonumero,osasto,toimi):
        nimiflag,hnkflag,osflag,tflag = False,False,False,False

        while nimiflag == False:
            if len(nimi) == 0:
                nimi = input("Ole hyvä ja anna nimi uudestaan")
                if len(nimi) == 0:
                    continue
            else:
                    self.__nimi = nimi
                    nimiflag=True

        while hnkflag == False:
            if len(str(henkilonumero)) != 5:
                try:
                    henkilonumero = int(input("Ole hyvä ja anna henkilänumero uudestaan"))
                    continue
                except ValueError:
                    continue
            else:
                    self.__henkilonumero = henkilonumero
                    hnkflag=True
        while osflag == False:
            if len(osasto) == 0:
                osasto = input("Ole hyvä ja anna osaston nimi uudestaan")
                if len(osasto) == 0:
                    continue
            else:
                    self.__osasto=osasto
                    osflag = True
        while tflag == False:
            if len(toimi) == 0:
                toimi = input("Ole hyvä ja anna toimen nimi uudestaan")
                if len(toimi) == 0:
                    continue
            else:
                    self.__toimi = toimi
                    tflag = True
    def __str__(self):
        return 'Henkiönumero:' + format(self.__henkilonumero) + 'Nimi' + format(self.__nimi) + 'Osasto' + format(self.__osasto) + 'Toimi' +format(self.__toimi)

    def set__nimi(self,nimi):
        nimiflag = False
        while nimiflag == False:
                if len(nimi) == 0:
                    nimi = input("Ole hyvä ja anna nimi uudestaan")
                    if len(nimi) == 0:
                        continue
                    else:
                        self.__nimi = nimi
                        nimiflag=True

    def set__henkilonumero(self,henkilonumero):
        hnkflag = False
        while hnkflag == False:
            if len(henkilonumero) == 0:
                try:
                    henkilonumero = int(input("Ole hyvä ja anna henkilonumero uudestaan"))
                    if len(henkilonumero) != 0:
                        continue
                except ValueError:
                    continue
                else:
                    self.__henkilonumero = henkilonumero
                    hnkflag=True
    def set__osasto(self,osasto):
        while osflag == False:
            if len(osasto) == 0:
                osasto = input("Ole hyvä ja anna osaston nimi uudestaan")
                if len(osasto) == 0:
                    continue
                else:
                    self.__osasto=osasto
                    osflag = False
    def set__toimi(self,toimi):
        while tflag == False:
            if len(toimi) == 0:
                toimi = input("Ole hyvä ja anna toimen nimi uudestaan")
                if len(toimi) == 0:
                    continue
                else:
                    self.__toimi = toimi
                    tflag = True
    def get__nimi(self):
        return self.__nimi
    def get__osasto(self):
        return self.__osasto
    def get__henkilonumero(self):
        return self.__henkilonumero
    def get__toimi(self):
        return self.__toimi


def loaddictionary():
    try:
        with open('tyontekijat.dat','rb') as file:
            osoitteet =pickle.load(file)
            if osoitteet == None:
                return {}
            return osoitteet
    except IOError:
        return {}
    except EOFError:
        return {}

def addemployee(sanakirja):
    employee = Tyontekija(input('Anna työntekijän nimi'),int(input('Anna henkilönumero')),input('Anna osasto'),input('Anna toimi'))
    henkilonumero = employee.get__henkilonumero()
    nimi = employee.get__nimi()
    osasto = employee.get__osasto()
    toimi = employee.get__toimi()
    sanakirja[henkilonumero] = nimi,osasto,toimi
    return sanakirja

def changeemployee(sanakirja,etsittava):
    del sanakirja[etsittava]
    employee = Tyontekija(input('Anna nimi'),etsittava,input('Anna osasto'),input('Anna toimi'))
    henkilonumero = employee.get__henkilonumero()
    nimi = employee.get__nimi()
    osasto = employee.get__osasto()
    toimi = employee.get__toimi()
    sanakirja[henkilonumero] = nimi,osasto,toimi

    return sanakirja

def dumpdictionary(osoitteet):
    with open('tyontekijat.dat','wb') as file:
        pickle.dump(osoitteet,file)
#########Tehtävä 1###########
from classes import Lemmikki
def main():
        lemmikki1 = Lemmikki(input('Anna eläimen nimi:'),input('Anna eläinlaji'),int(input("Anna eläimen ikä")))

        print("Eläimen nimi: {0}, Eläimen laji: {1}, Eläimen ikä {2}".format(lemmikki1.get__nimi(),lemmikki1.get__elainlaji(),lemmikki1.get__ika()))


main()
#########Tehtävä2###########
from classes import Auto

def main():
    mercedes = Auto(1988,'Mercedes')
    kiihdytys = 5
    jarrutus = 5
    i = 0
    while i < 5:
            mercedes.kiihdyta()
            print("Auton vauhti tällähetkellä: {0}".format(mercedes.get__nopeus()))
            i+=1
    i = 0
    while i < jarrutus:
        mercedes.jarruta()
        print("Auton vauhti tällähetkellä: {0}".format(mercedes.get__nopeus()))
        i+=1


main()

###########Tehtävä 3######################
####Tehtävä 3 on classes.py tiedostossa oleva luokka########



#############Tehtävä 4##############
from classes import Tyontekija, loaddictionary, addemployee,changeemployee,dumpdictionary
import os


def main():
    sanakirja = {}
    sanakirja = loaddictionary()
    continueflag = True
    print("Kuinka tätä ohjelmaa käytetään")
    print("Paina 1 etsiäksesi työntekijää")
    print("Paina 2 lisätäksesi työntekijä")
    print("Paina 3 muuttaaksesi työntekijän tietoja")
    print("Paina 4 poistaaksesi työntekijä")
    print("Paina 5 listataksesi kaikki työntekijät")
    print("Paina 6 lopettaaksesi ohjelma")
    while continueflag == True:
        choice = int(input("Anna valintasi"))
        if choice == 1:
            etsittava = int(input("Anna etsittavan henkilönumero"))
            if etsittava in sanakirja:
                print("Sanakirjasta löytyi henkilö: {0}".format(etsittava))
                print("Nimi     Osasto      Työtehtävä")
                for value in sanakirja[etsittava]:
                        print(value,end=' ')
            else:
                print("Etsittävä henkilö ei löytynyt sanakirjasta")
        elif choice == 2:
            sanakirja = addemployee(sanakirja)
        elif choice == 3:
            etsittava = int(input("Anna muutettavan henkilönumero:"))
            if etsittava in sanakirja:
                sanakirja = changeemployee(sanakirja,etsittava)
            else:
                print("Antamaasi henkilönumeroa ei löytynyt")
        elif choice == 4:
            etsittava = int(input("Anna poistettavan työntekijän henkilönumero:"))
            if etsittava in sanakirja:
                print("Sanakirjasta poistettu henkilö: {0}".format(etsittava))
                del sanakirja[etsittava]
            else:
                print("Etsittävä henkilö ei löytynyt sanakirjasta")
        elif choice == 5:
            os.system('cls')
            for i in sanakirja.keys():
                for value in sanakirja[i]:
                    print(value,end=' ')
                print("\n-----------------------------------------------------------")

        elif choice ==  6:
            dumpdictionary(sanakirja)
            continueflag = False



main()















