import pickle

class Lemmikki:
    def __init__(self,nimi,elainlaji,ika):
        self.__nimi = nimi
        self.__elainlaji = elainlaji
        self.__ika = ika
    def set__nimi(self,nimi):
        self.__nimi = nimi
    def set__elainlaji(self,elainlaji):
        self.__elainlaji = elainlaji
    def set_ika(self,ika):
        self.__ika = ika
    def get__nimi(self):
        return self.__nimi
    def get__elainlaji(self):
        return self.__elainlaji
    def get__ika(self):
        return self.__ika


class Auto:
    def __init__(self,vuosimalli,malli):
        self.__vuosimalli = vuosimalli
        self.__malli = malli
        self.__nopeus = 0
    def kiihdyta(self):
        self.__nopeus += 5
    def jarruta(self):
        self.__nopeus -= 5
    def get__nopeus(self):
        return self.__nopeus

class Tyontekija:
    def __init__(self,nimi,henkilonumero,osasto,toimi):
        nimiflag,hnkflag,tflag,osflag = False, False, False, False
        while nimiflag == False:
            if len(nimi) == 0:
                nimi = input("Ole hyvÃƒÂ¤ ja anna nimi uudestaan")
                if len(nimi) == 0:
                    continue
            else:
                    self.__nimi = nimi
                    nimiflag=True

        while hnkflag == False:
            if len(str(henkilonumero)) != 5:
                try:
                    henkilonumero = int(input("Ole hyvÃƒÂ¤ ja anna henkilÃƒÂ¤numero uudestaan"))
                    continue
                except ValueError:
                    continue
            else:
                    self.__henkilonumero = henkilonumero
                    hnkflag=True
        while osflag == False:
            if len(osasto) == 0:
                osasto = input("Ole hyvÃƒÂ¤ ja anna osaston nimi uudestaan")
                if len(osasto) == 0:
                    continue
            else:
                    self.__osasto=osasto
                    osflag = True
        while tflag == False:
            if len(toimi) == 0:
                toimi = input("Ole hyvÃƒÂ¤ ja anna toimen nimi uudestaan")
                if len(toimi) == 0:
                    continue
            else:
                    self.__toimi = toimi
                    tflag = True
    def __str__(self):
        return 'HenkiÃƒÂ¶numero:' + format(self.__henkilonumero) + 'Nimi' + format(self.__nimi) + 'Osasto' + format(self.__osasto) + 'Toimi' +format(self.__toimi)

    def set__nimi(self,nimi):
        nimiflag = False
        while nimiflag == False:
                if len(nimi) == 0:
                    nimi = input("Ole hyvÃƒÂ¤ ja anna nimi uudestaan")
                    if len(nimi) == 0:
                        continue
                else:
                        self.__nimi = nimi
                        nimiflag=True

    def set__henkilonumero(self,henkilonumero):
        hnkflag = False
        while hnkflag == False:
            if len(henkilonumero) == 0:
                try:
                    henkilonumero = int(input("Ole hyvÃƒÂ¤ ja anna henkilonumero uudestaan"))
                    if len(henkilonumero) != 0:
                        continue
                except ValueError:
                    continue
                else:
                    self.__henkilonumero = henkilonumero
                    hnkflag=True
    def set__osasto(self,osasto):
        osflag = False
        while osflag == False:
            if len(osasto) == 0:
                osasto = input("Ole hyvÃƒÂ¤ ja anna osaston nimi uudestaan")
                if len(osasto) == 0:
                    continue
            else:
                    self.__osasto=osasto
                    osflag = True
    def set__toimi(self,toimi):
        tflag = False
        while tflag == False:
            if len(toimi) == 0:
                toimi = input("Ole hyvÃƒÂ¤ ja anna toimen nimi uudestaan")
                if len(toimi) == 0:
                    continue
            else:
                    self.__toimi = toimi
                    tflag = True
    def get__nimi(self):
        return self.__nimi
    def get__osasto(self):
        return self.__osasto
    def get__henkilonumero(self):
        return self.__henkilonumero
    def get__toimi(self):
        return self.__toimi


def loaddictionary():
    try:
        with open('tyontekijat.dat','rb') as file:
            osoitteet =pickle.load(file)
            if osoitteet == None:
                return {}
            return osoitteet
    except IOError:
        return {}
    except EOFError:
        return {}

def addemployee(sanakirja):
    henkilonumero = int(input('Anna henkilÃƒÂ¶numero'))
    employee = Tyontekija(input('Anna tyÃƒÂ¶ntekijÃƒÂ¤n nimi'),henkilonumero,input('Anna osasto'),input('Anna toimi'))
    sanakirja[employee.get__henkilonumero()] = employee
    return sanakirja

def changeemployee(sanakirja,etsittava):
    sanakirja[etsittava].set__nimi(input('Anna työntekijän nimi'))
    sanakirja[etsittava].set__osasto(input('Anna työntekijän osasto'))
    sanakirja[etsittava].set__toimi(input('Anna työntekijän toimi'))
    return sanakirja

def dumpdictionary(osoitteet):
    with open('tyontekijat.dat','wb') as file:
        pickle.dump(osoitteet,file)












