# Esimerkki 12-9 luokat NisÃ¤kÃ¤s, Koira ja Kissa
# tiedosto elaimet.py

class Nisakas:

    # alustaja saa argumenttina lajin.
    def __init__(self, laji):
        self.__laji = laji

    # metodi nayta_laji kertoo nisÃ¤kkÃ¤Ã¤n lajin
    def nayta_laji(self):
        print('Olen ', self.__laji)

    # metodi Ã¤Ã¤ntele kertoo miten nisÃ¤kÃ¤s Ã¤Ã¤ntelee
    def aantele(self):
        print('Grrrr')


# Luokka Koira on luokan NisÃ¤kÃ¤s aliluokka
class Koira():

    # __init__ kutsuu yliluokan alustajaa
    def __init__(self):
        Nisakas.__init__(self, 'Koira')

    # korvataan yliluokan Ã¤Ã¤ntele metodi
    def aantele(self):
        print('Vou! Vou!')

#Luokka Kissa  on luokan NisÃ¤kÃ¤n aliluokka

class Kissa(Nisakas):

    # __init__ kutsuu yliluokan alustajaa
    def __init__(self):
        Nisakas.__init__(self, 'Kissa')

    # korvataan yliluokan Ã¤Ã¤ntele metodi
    def aantele(self):
        print('Miauuu')
class suomenpystykorva(Koira):
    def aantele(self):
        print("Jooh")
