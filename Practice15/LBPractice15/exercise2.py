class Kasvi:
    def __init__(self, kasvityyppi):
        self.__kasvityyppi = kasvityyppi

    def viesti(self):
        print("Olen kasvi")

class Puu(Kasvi):
    def __init__(self):
        Kasvi.__init__(self,'puu')

    def viesti(self):
        print("Olen muuten puu")


def main():
    p = Kasvi('taimi')
    t = Puu()
    p.viesti()
    t.viesti()
    u = p
    p = tt = u
    p.viesti()
    t.viesti()
main()
