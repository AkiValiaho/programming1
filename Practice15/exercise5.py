globkutsut= 0
globcalls = 0


def ackermann(m,n):
    global globkutsut
    globkutsut+=1
    if m == 0:
        return n+1
    elif n == 0:
        return ackermann(m-1,1)
    else:
       return ackermann(m-1,ackermann(m,n-1))

def main():
    print(ackermann(int(input('Anna m')),int(input('Anna n'))))
    print(globkutsut)
    print(iterative_ackermann(int(input('Anna m')),int(input('Anna n'))))
    print(globcalls)


############Tarvitsee siis noin globkutsut*2 verran muistipaikkoja, arvoilla 2,2 tarvitsee 54 muistipaikkaa

############osittain ei-rekursiivinen ohjelma:###################################################
############Tämä puolittaa rekursion kutsujen määrän ja siten myös muistissa varattujen paikkojen määrän###

def iterative_ackermann(m, n):
    global globcalls
    globcalls += 1
    while m != 0:
        if n == 0:
            n = 1
        else:
            n = iterative_ackermann(m, n - 1)
        m -= 1
    return n + 1

main()
