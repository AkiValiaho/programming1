﻿import pickle

class bmi:
    def __init__(self,nimi,pituus,paino):
        self.__nimi = nimi
        self.__pituus = pituus
        self.__paino = paino
        self.__bmi = paino / pituus**2
    def get__nimi(self):
        return self.__nimi
    def get__bmi(self):
        return self.__bmi
    def get__Tulkinta(self):
        if self.__bmi < 18.5:
            return 'Olet alipainoinen'
        elif self.__bmi > 18.5 and self.__bmi <25:
            return 'Olet normaalipainoinen'
        elif self.__bmi > 25 and self.__bmi <30:
            return 'Olet lievästi ylipainoinen'
        elif self.__bmi > 30:
            return 'Olet merkittävästi ylipainoinen'
    def __str__(self):
        return 'Nimesi: ' + format(self.__nimi) + ' BMI:si: {0:.2f}'.format(self.get__bmi()) + ' Tulkinta:' + format(self.get__Tulkinta())

class Tyontekija:
    def __init__(self,nimi,henkilonumero,osasto,toimi):
        self.__nimi = nimi
        self.__henkilonumero = henkilonumero
        self.__osasto = osasto
        self.__toimi = toimi

    def get__nimi(self):
        return self.__nimi

    def get__henkilonumero(self):
        return self.__henkilonumero
    def get__osasto(self):
        return self.__osasto
    def get__toimi(self):
        return self.__toimi
    def set__nimi(self,nimi):
        self.__nimi = nimi

    def set__henkilonumero(self,numero):
        self.__henkilonumero = henkilonumero
    def set__osasto(self,osasto):
        self.__osasto = osasto
    def set__toimi(self,toimi):
        self.__toimi = toimi
    def __str__(self):
        return 'Nimi: {0} Henkilönumero: {1} Osasto: {2} Toimi: {3}'.format(self.get__nimi(),self.get__henkilonumero(),self.get__osasto(),self.get__toimi())

class Vuorotyolainen(Tyontekija):
    def __init__(self):
        Tyontekija.__init__(self,input('Anna nimi'),int(input('Anna henkilönumero')),input('Anna osasto'),input('Anna toimi'))
        self.__vuoronumero = int(input('Anna henkilön vuoronumero'))
        self.__palkka = float(input('Anna henkilön palkka'))
    def set__vuoronumero(self):
        self.__vuoronumero = vuoronumero
    def set__palkka(self):
        self.__palkka = palkka
    def get__vuoronumero(self):
        return self.__vuoronumero
    def get__palkka(self):
        return self.__palkka
    def __str__(self):
        return 'Nimi: {0} Henkilönumero: {1} Osasto: {2} Toimi: {3} Vuoronumero: {4} Palkka: {5}'.format(self.get__nimi(),self.get__henkilonumero(),self.get__osasto(),self.get__toimi(),self.__vuoronumero,self.__palkka)

class Vuoropaallikko(Vuorotyolainen):
    def __init__(self):
        Vuorotyolainen.__init__(self)
        self.__bonuspalkka = float(input('Anna bonuspalkka'))
    def set__bonuspalkka(self,bonuspalkka):
        self.__bonuspalkka = bonuspalkka
    def get__bonuspalkka(self):
        return self.__bonuspalkka
    def __str__(self):
        return super().__str__() + ' Kertyneet bonukset {0}'.format(self.__bonuspalkka)

def addemployee(sanakirja,choice):
    if choice == 1:
        henkilonumero = int(input('Anna henkilönumero'))
        employee = Tyontekija(input('Anna työntekijän nimi'),henkilonumero,input('Anna osasto'),input('Anna toimi'))
        sanakirja[employee.get__henkilonumero()] = employee
        return sanakirja
    elif choice == 2:
        employee = Vuorotyolainen()
        sanakirja[employee.get__henkilonumero()] = employee
        return sanakirja
    elif choice == 3:
        employee = Vuoropaallikko()
        sanakirja[employee.get__henkilonumero()] = employee
        return sanakirja

def dumpdictionary(osoitteet):
    with open('tyontekijat.dat','wb') as file:
        pickle.dump(osoitteet,file)
def loaddictionary():
    try:
        with open('tyontekijat.dat','rb') as file:
            osoitteet =pickle.load(file)
            if osoitteet == None:
                return {}
            return osoitteet
    except IOError:
        return {}
    except EOFError:
        return {}
def changeemployee(sanakirja,etsittava):
    sanakirja[etsittava].set__nimi(input('Anna työntekijän nimi'))
    sanakirja[etsittava].set__osasto(input('Anna työntekijän osasto'))
    sanakirja[etsittava].set__toimi(input('Anna työntekijän toimi'))
    return sanakirja
