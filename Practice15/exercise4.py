﻿from classes import Tyontekija,Vuorotyolainen,Vuoropaallikko,addemployee,loaddictionary,dumpdictionary,changeemployee

def main():
    continueflag = True
    sanakirja = {}
    sanakirja = loaddictionary()
    while continueflag == True:
        print("Paina 1 etsiäksesi työntekijän listalta")
        print("Paina 2 lisätäksesi uuden työntekijän listalle")
        print("Paina 3 muuttaaksesi työntekijän tietoja listalta")
        print("Paina 4 poistaaksesi työntekijän sanakirjasta")
        print("Paina 5 listataksesi kaikki työntekijän")
        print("Paina 6 lopettaaksesi ohjelman käytön")
        choice = int(input("Anna valintasi"))
        if choice == 1:
            etsittava = int(input("Anna etsittavan henkilönumero"))
            if etsittava in sanakirja:
                print("Sanakirjasta löytyi henkilö: {0}".format(etsittava))
                print(sanakirja[etsittava].__str__())

            else:
                print("Etsittävä henkilö ei löytynyt sanakirjasta")
        elif choice == 2:
            luokka = int(input("1. Työntekijä 2. Vuorotyöläinen 3. Vuoropäällikkö"))
            addemployee(sanakirja,luokka)
        elif choice == 3:
            etsittava = int(input("Anna muutettavan henkilönumero:"))
            if etsittava in sanakirja:
                sanakirja = changeemployee(sanakirja,etsittava)
            else:
                print("Antamaasi henkilönumeroa ei löytynyt listalta")
        elif choice == 4:
            etsittava = int(input("Anna poistettavan työntekijän henkilönumero"))
            if etsittava in sanakirja:
                print("Sanakirjasta poistettu henkilö: {0}".format(etsittava))
                del sanakirja[etsittava]
            else:
                print("Etsittävä henkilö ei löytynyt sanakirjasta")
        elif choice == 5:
                for i in sanakirja.keys():
                    print(sanakirja[i].__str__())
                    print("\n-----------------------------------------------------------")
        elif choice == 6:
            dumpdictionary(sanakirja)
            continueflag = False
main()

